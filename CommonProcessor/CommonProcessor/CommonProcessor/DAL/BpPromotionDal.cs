﻿using CommonProcessor.Models.DbContext;
using CommonProcessor.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CommonProcessor.DAL
{
    public class BpPromotionDal
    {
        #region Common
        private readonly BrandPromotionEntities _context;

        public BpPromotionDal(BrandPromotionEntities context)
        {
            _context = context;
        }


        private SqlConnection GetConnectionSql()
        {
            //var connectionStringName = ConfigurationManager.ConnectionStrings["RBSYNERGYEntities"];
            //string connectionString = connectionStringName.ConnectionString;
            const string connectionString = @"Data Source = 192.168.119.83; Initial Catalog = BrandPromotion; user id=SA;password=SGR~1806*20;";
            try
            {
                SqlConnection conCheck = new SqlConnection(connectionString);
                return new SqlConnection(connectionString);
            }
            catch (Exception e)
            {
                throw new Exception("Error : " + e.Message);
            }
        }


        #endregion

        public async Task<List<BpSalesReportViewModel>> GetBpSalesReportData(DateTime fromDate, DateTime todate)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" With cte As( 
        SELECT row_number() OVER(PARTITION BY sales.IMEI  ORDER BY sales.IMEI desc ) AS [rn],sales.Imei,case when b.Barcode2 is null then previous.BarCode2 else b.BarCode2  end as Imei2, 
        sales.Model,price.MSRP,sales.Claim_Date,info.BPName,sales.BpPhoneMumber,sales.RetailerName,info.RetailerAddress,sales.DealerCode,info.AlternateDistributorCode,sales.DealerName,
        case when repI.Imei_1 is null then '' else 'Replacement Issue'  end as Replacement ,CAST(r.RegistrationDate AS DATE) AS RegistrationDate
        FROM (Select Imei,Model,CAST(AddedDate AS DATE) AS Claim_Date,BpPhoneMumber,RetailerName,DealerCode,DealerName 
        from BrandPromotion.[dbo].BPSales WHERE CAST(AddedDate AS DATE) BETWEEN '{0}' AND '{1}' AND IsValidOffer = 1)sales
        LEFT OUTER JOIN (select Barcode,BarCode2 from [RBSYNERGY].[dbo].tblDealerDistributionDetails) b ON sales.Imei = b.BarCode 
        LEFT OUTER JOIN (select Barcode,BarCode2 from [RB_BACK_DATED].[dbo].tblDealerDistributionDetails) previous ON sales.Imei = previous.BarCode
        LEFT OUTER JOIN [RBSYNERGY].[dbo].[tblProductRegistration] r ON sales.Imei = r.Imei_One
        LEFT OUTER JOIN [RBSYNERGY].[dbo].[tblCellPhonePricing] price on sales.Model=price.Model
        LEFT OUTER JOIN BrandPromotion.[dbo].BPInfo info ON sales.BpPhoneMumber = info.BPPhoneNumber 
        LEFT OUTER JOIN (Select distinct rep.IMEI_1 from [RBSYNERGY].[IMEIReplacementMaster].[IMEIReplacementMaster] rep 
        INNER JOIN BrandPromotion.[dbo].BPSales sale on rep.IMEI_1=sale.Imei
        where RequestStatus in ('WastageReceived','DeliveredToRework','ReworkReceived','Approved')
        ) repI on sales.Imei=repI.IMEI_1
        LEFT JOIN BrandPromotion.[dbo].MOInfo moitor on sales.DealerCode=moitor.DistributorCode or sales.DealerCode=moitor.AlternateDistributorCode
        union 

        SELECT row_number() OVER(PARTITION BY sales.IMEI  ORDER BY sales.IMEI desc ) AS [rn],sales.Imei,case when b.Barcode2 is null then previous.BarCode2 else b.BarCode2  end as Imei2, 
        sales.Model,price.MSRP,sales.Claim_Date,info.BPName,sales.BpPhoneMumber,sales.RetailerName,info.RetailerAddress,sales.DealerCode,info.AlternateDistributorCode,sales.DealerName, 
        case when repI.Imei_2 is null then '' else 'Replacement Issue'  end as Replacement,CAST(r.RegistrationDate AS DATE) AS RegistrationDate
        FROM 
	        (Select Imei,Model,CAST(AddedDate AS DATE) AS Claim_Date,BpPhoneMumber,RetailerName,DealerCode,DealerName 
		        from BrandPromotion.[dbo].BPSales WHERE CAST(AddedDate AS DATE) BETWEEN '{0}' AND '{1}' AND IsValidOffer = 1)sales 
        LEFT OUTER JOIN (select Barcode,BarCode2 from [RBSYNERGY].[dbo].tblDealerDistributionDetails) b ON sales.Imei = b.BarCode2 
        LEFT OUTER JOIN (select Barcode,BarCode2 from [RB_BACK_DATED].[dbo].tblDealerDistributionDetails) previous ON sales.Imei = previous.BarCode2
        LEFT OUTER JOIN [RBSYNERGY].[dbo].[tblProductRegistration] r ON sales.Imei = r.Imei_Tow
        LEFT OUTER JOIN [RBSYNERGY].[dbo].[tblCellPhonePricing] price on sales.Model=price.Model
        LEFT OUTER JOIN BrandPromotion.[dbo].BPInfo info ON sales.BpPhoneMumber = info.BPPhoneNumber 
        LEFT OUTER JOIN  
	        (Select distinct rep.IMEI_2 from [RBSYNERGY].[IMEIReplacementMaster].[IMEIReplacementMaster] rep  
		        INNER JOIN BrandPromotion.[dbo].BPSales sale on rep.IMEI_1=sale.Imei
		        where RequestStatus in ('WastageReceived','DeliveredToRework','ReworkReceived','Approved')
	        ) repI on sales.Imei=repI.IMEI_2
        LEFT JOIN BrandPromotion.[dbo].MOInfo moitor on sales.DealerCode=moitor.DistributorCode or sales.DealerCode=moitor.AlternateDistributorCode
        )Select * from cte WHERE CTE.Imei2 IS NOT NULL   ", fromDate, todate);
                var data = await _context.Database.SqlQuery<BpSalesReportViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public async Task<BPInfo> GetABpByMobileNumber(string BPPhoneNumber)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT * FROM [BrandPromotion].[dbo].[BPInfo] where BPPhoneNumber='{0}' ", BPPhoneNumber);
                var data = await _context.Database.SqlQuery<BPInfo>(query).FirstOrDefaultAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public async Task<int> AddOrEditBP(string EbsCode, string DealerCode, string DealerName, string District, string RetailerID, string RetailerName, string RetailerAddress, string RetailerPhoneNumber, string BpPhoneNumber, string BpName, bool IsActive)
        {
            var data = await GetABpByMobileNumber(BpPhoneNumber);

            if (DealerName == "Cox's Bazar Mobile Center- 2-CELLCOM")
            {
                DealerName = "Cox''s Bazar Mobile Center- 2-CELLCOM";
            }
            if (District == "Cox'S Bazar")
            {
                District = "Cox''S Bazar";
            }

            if (data == null)
            {
                _context.Database.CommandTimeout = 6000;
                string Sql = " Insert into [BrandPromotion].[dbo].[BPInfo] " +
                                "(District,DistributorName,DistributorCode,AlternateDistributorCode,RetailerID,RetailerName,RetailerAddress,RetailerPhoneNumber,BPName,BPPhoneNumber,Remarks,Active,CreatedBy) " +
                                "values ('" + District + "','" + DealerName + "','" + DealerCode + "','" + EbsCode + "','" + RetailerID + "','" + RetailerName + "','" + RetailerAddress + "','" + RetailerPhoneNumber + "','" + BpName + "','" + BpPhoneNumber + "',convert(varchar, getdate(), 23),'" + IsActive + "','18509') ";

                var savedata = await _context.Database.ExecuteSqlCommandAsync(Sql);
                return savedata;
            }
            else
            {
                _context.Database.CommandTimeout = 6000;
                string Sql = " Update [BrandPromotion].[dbo].[BPInfo] set Active='" + IsActive + "',CreatedBy='18509' where BPPhoneNumber='" + BpPhoneNumber + "' ";

                var savedata = await _context.Database.ExecuteSqlCommandAsync(Sql);
                return savedata;
            }


        }
        public async Task<List<ActiveBPListViewModel>> GetAllActiveBP()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" ;WITH CTE AS(
	                                                        SELECT 
	                                                           ROW_NUMBER() OVER(PARTITION BY bpphonenumber ORDER BY bpphonenumber) AS [rn]
	                                                          ,[District]
                                                              ,[DistributorName]
                                                              ,[DistributorCode]
	                                                          ,[AlternateDistributorCode]
                                                              ,[RetailerName]
	                                                          ,[RetailerAddress]
                                                              ,bpname
                                                              ,bpphonenumber
                                                          FROM BrandPromotion.[dbo].[BPInfo]
                                                          WHERE [Active] = 1 AND [DistributorCode] IS NOT NULL AND bpphonenumber IS NOT NULL AND [DistributorCode]<>'' 
                                                          AND LEN(bpphonenumber)=11 
                                                        )
                                                        SELECT * FROM CTE WHERE [rn] = 1 ORDER BY bpphonenumber ");
                var data = await _context.Database.SqlQuery<ActiveBPListViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                //ExceptionLogging.SendErrorToText(e);
                return null;
            }

        }
        public async Task<List<BpSalesReportViewModel>> GetBpWiseSalesReportData(DateTime fromDate, DateTime todate, string ddlPhn)
        {
            try
            {
                string[] bpphn;
                string whereExt = string.Empty;
                if (!string.IsNullOrWhiteSpace(ddlPhn))
                {
                    bpphn = ddlPhn.Split(',');
                    string z = string.Empty;
                    foreach (var s in bpphn)
                    {
                        z = z + "'" + s.Trim() + "',";
                    }
                    z = z.TrimEnd(',');
                    whereExt = "AND cte.BpPhoneMumber in (" + z + ")";
                }

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" With cte As( 
                                                            SELECT row_number() OVER(PARTITION BY sales.IMEI  ORDER BY sales.IMEI desc ) AS [rn],sales.Imei,case when b.Barcode2 is null then previous.BarCode2 else b.BarCode2  end as Imei2, 
                                                            sales.Model,ISNULL(price.MSRP,0)MSRP,sales.Claim_Date,info.BPName,sales.BpPhoneMumber,sales.RetailerName,info.RetailerAddress,sales.DealerCode,sales.DealerName,
                                                            case when repI.Imei_1 is null then '' else 'Replacement Issue'  end as Replacement 
                                                            FROM (Select Imei,Model,CAST(AddedDate AS DATE) AS Claim_Date,BpPhoneMumber,RetailerName,DealerCode,DealerName 
                                                            from BrandPromotion.[dbo].BPSales WHERE CAST(AddedDate AS DATE) BETWEEN '{0}' AND '{1}' AND IsValidOffer = 1)sales
                                                            LEFT OUTER JOIN (select Barcode,BarCode2 from [RBSYNERGY].[dbo].tblDealerDistributionDetails) b ON sales.Imei = b.BarCode 
                                                            LEFT OUTER JOIN (select Barcode,BarCode2 from [RB_BACK_DATED].[dbo].tblDealerDistributionDetails) previous ON sales.Imei = previous.BarCode
                                                            LEFT OUTER JOIN [RBSYNERGY].[dbo].[tblCellPhonePricing] price on sales.Model=price.Model
                                                            LEFT OUTER JOIN BrandPromotion.[dbo].BPInfo info ON sales.BpPhoneMumber = info.BPPhoneNumber 
                                                            LEFT OUTER JOIN (Select distinct rep.IMEI_1 from [RBSYNERGY].[IMEIReplacementMaster].[IMEIReplacementMaster] rep 
                                                            INNER JOIN BrandPromotion.[dbo].BPSales sale on rep.IMEI_1=sale.Imei
                                                            where RequestStatus in ('WastageReceived','DeliveredToRework','ReworkReceived','Approved')
                                                            ) repI on sales.Imei=repI.IMEI_1 
                                                            union 

                                                            SELECT row_number() OVER(PARTITION BY sales.IMEI  ORDER BY sales.IMEI desc ) AS [rn],sales.Imei,case when b.Barcode2 is null then previous.BarCode2 else b.BarCode2  end as Imei2, 
                                                            sales.Model,ISNULL(price.MSRP,0)MSRP,sales.Claim_Date,info.BPName,sales.BpPhoneMumber,sales.RetailerName,info.RetailerAddress,sales.DealerCode,sales.DealerName, 
                                                            case when repI.Imei_2 is null then '' else 'Replacement Issue'  end as Replacement
                                                            FROM (Select Imei,Model,CAST(AddedDate AS DATE) AS Claim_Date,BpPhoneMumber,RetailerName,DealerCode,DealerName 
                                                            from BrandPromotion.[dbo].BPSales WHERE CAST(AddedDate AS DATE) BETWEEN '{0}' AND '{1}' AND IsValidOffer = 1)sales 
                                                            LEFT OUTER JOIN (select Barcode,BarCode2 from [RBSYNERGY].[dbo].tblDealerDistributionDetails) b ON sales.Imei = b.BarCode2 
                                                            LEFT OUTER JOIN (select Barcode,BarCode2 from [RB_BACK_DATED].[dbo].tblDealerDistributionDetails) previous ON sales.Imei = previous.BarCode2
                                                            LEFT OUTER JOIN [RBSYNERGY].[dbo].[tblCellPhonePricing] price on sales.Model=price.Model
                                                            LEFT OUTER JOIN BrandPromotion.[dbo].BPInfo info ON sales.BpPhoneMumber = info.BPPhoneNumber 
                                                            LEFT OUTER JOIN  (Select distinct rep.IMEI_2 from [RBSYNERGY].[IMEIReplacementMaster].[IMEIReplacementMaster] rep  
                                                            INNER JOIN BrandPromotion.[dbo].BPSales sale on rep.IMEI_1=sale.Imei
                                                            where RequestStatus in ('WastageReceived','DeliveredToRework','ReworkReceived','Approved')
                                                            ) repI on sales.Imei=repI.IMEI_2 

                                                        )Select * from cte WHERE CTE.Imei2 IS NOT NULL {2}  ", fromDate, todate, whereExt);
                var data = await _context.Database.SqlQuery<BpSalesReportViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<BpWiseSalesQuantityViewModel>> GetBPWiseSalesQtyReport(DateTime fromDate, DateTime todate)
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select bs.DealerCode,bs.DealerName,bs.BpPhoneMumber,bi.BPName,COUNT(bs.Imei) SALES_QTY from BrandPromotion.dbo.BPSales  bs
                                                inner join RBSYNERGY.dbo.tblProductRegistration pr on bs.Imei=pr.Imei_One
                                                inner join BrandPromotion.dbo.BPInfo bi on bs.BpPhoneMumber=bi.BPPhoneNumber
                                                where CAST(pr.RegistrationDate as date) between '{0}' and '{1}' 
                                                and CAST(bs.AddedDate as date) between '{0}' and '{1}'
                                                and bi.Active=1 --and bs.BpPhoneMumber='01980545559'
                                                Group by bs.DealerCode,bs.DealerName,bs.BpPhoneMumber,bi.BPName  ", fromDate, todate);
                var data = await _context.Database.SqlQuery<BpWiseSalesQuantityViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }
    }
}