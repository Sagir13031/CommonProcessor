﻿using CommonProcessor.Models.DbContext;
using CommonProcessor.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CommonProcessor.DAL
{
    public class MDMSDal
    {
        #region Common
        private readonly MDSEntities _context;

        public MDMSDal(MDSEntities context)
        {
            _context = context;
        }


        private SqlConnection GetConnectionSql()
        {
            //var connectionStringName = ConfigurationManager.ConnectionStrings["RBSYNERGYEntities"];
            //string connectionString = connectionStringName.ConnectionString;
            const string connectionString = @"Data Source =103.147.182.250; Initial Catalog = MDS; user id=sa;password=Wal_ton@20@20;";
            try
            {
                SqlConnection conCheck = new SqlConnection(connectionString);
                return new SqlConnection(connectionString);
            }
            catch (Exception e)
            {
                throw new Exception("Error : " + e.Message);
            }
        }


        #endregion

        public int ProcessImeiDate(List<WDMS_Dealer_DistributionViewModel> dealerList)
        {
            int copyData = 0;
            try
            {
                truncateQueryTable();//Frist Truncate QueryTable Then Add Data
                DataTable tbl = new DataTable();
                tbl.Columns.Add(new DataColumn("DealerCode", typeof(string)));
                tbl.Columns.Add(new DataColumn("Model", typeof(string)));
                tbl.Columns.Add(new DataColumn("ImeiOne", typeof(string)));
                tbl.Columns.Add(new DataColumn("ImeiTwo", typeof(string)));
                tbl.Columns.Add(new DataColumn("Color", typeof(string)));
                tbl.Columns.Add(new DataColumn("DistributionDate", typeof(string)));
                tbl.Columns.Add(new DataColumn("AddedBy", typeof(string)));

                for (int i = 0; i < dealerList.Count; i++)
                {
                    DataRow dr = tbl.NewRow();
                    dr["DealerCode"] = dealerList[i].DealerCode;
                    dr["Model"] = dealerList[i].Model;
                    dr["ImeiOne"] = dealerList[i].ImeiOne;
                    dr["ImeiTwo"] = dealerList[i].ImeiTwo;
                    dr["Color"] = dealerList[i].Color;
                    //dr["DistributionDate"] = dealerList[i].DistributionDate;
                    dr["DistributionDate"] = Convert.ToDateTime(dealerList[i].DistributionDate).ToString("yyyy-MM-dd");
                    dr["AddedBy"] = dealerList[i].AddedBy;
                    tbl.Rows.Add(dr);
                }
                using (SqlConnection con = GetConnectionSql())
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "[MDS].[dbo].[Query$]";

                        //[OPTIONAL]: Map the Excel columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("DealerCode", "DealerCode");
                        sqlBulkCopy.ColumnMappings.Add("Model", "Model");
                        sqlBulkCopy.ColumnMappings.Add("ImeiOne", "ImeiOne");

                        sqlBulkCopy.ColumnMappings.Add("ImeiTwo", "ImeiTwo");
                        sqlBulkCopy.ColumnMappings.Add("Color", "Color");

                        sqlBulkCopy.ColumnMappings.Add("DistributionDate", "DistributionDate");
                        sqlBulkCopy.ColumnMappings.Add("AddedBy", "AddedBy");

                        con.Open();
                        sqlBulkCopy.WriteToServer(tbl);
                        con.Close();
                        con.Dispose();

                        copyData = tbl.Rows.Count;
                    }
                }
                return copyData;
            }
            catch (Exception e)
            {
                return copyData;
            }

        }

        private int truncateQueryTable()
        {
            int rowsAffected = 0;
            try
            {
                _context.Database.CommandTimeout = 6000;
                string Sql = "TRUNCATE TABLE [MDS].[dbo].[Query$] ";

                rowsAffected = _context.Database.ExecuteSqlCommand(Sql);
                return rowsAffected;
            }
            catch (Exception e)
            {
                return rowsAffected;
            }

        }

        public async Task<List<WDMS_IMEI_EXIST>> GetExistData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select x.DealerCode dDealerCode, x.ImeiOne dImeiOne, ddd.DealerCode dddDealerCode, ddd.ReceiveDate from (
                                                    SELECT [DealerCode],[Model],ImeiOne,ImeiTwo,[Color]
                                                  FROM [MDS].[dbo].[Query$]  where ImeiOne in (
                                                        SELECT q.ImeiOne FROM [MDS].[dbo].[Query$] q
                                                      join
                                                        MDS.dbo.DealerDistribution d
                                                        on q.ImeiOne = d.ImeiOne 
                                                      )
                                                    )x
                                                    join MDS.dbo.DealerDistribution ddd
                                                    on x.ImeiOne = ddd.ImeiOne ");
                var data = await _context.Database.SqlQuery<WDMS_IMEI_EXIST>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<int> UpdateMDMSTableDate()
        {
            int rowsAffected = 0;
            _context.Database.CommandTimeout = 6000;
            string Sql = string.Format(@" UPDATE MDS.dbo.DealerDistribution set DealerCode = y.dDealerCode, ReceiveDate = getdate() - 1
                                            from(
                                            select x.DealerCode dDealerCode, x.ImeiOne dImeiOne, ddd.DealerCode dddDealerCode, ddd.ReceiveDate from (
                                                SELECT[DealerCode],[Model], ImeiOne, ImeiTwo,[Color]
                                                FROM [MDS].[dbo].[Query$] where ImeiOne in (
                                                    SELECT q.ImeiOne
                                                    FROM [MDS].[dbo].[Query$] q
                                                join
                                                    MDS.dbo.DealerDistribution d
                                                    on q.ImeiOne = d.ImeiOne
                                                        )
                                                    )x
                                            join MDS.dbo.DealerDistribution ddd
                                            on x.ImeiOne = ddd.ImeiOne
                                            )y
                                            where ImeiOne = y.dImeiOne ");
            rowsAffected = await _context.Database.ExecuteSqlCommandAsync(Sql);
            return rowsAffected;

        }

        public async Task<List<WDMS_INSERT_IMEIViewModel>> GetAllIMEIDataForMDMS()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@"  SELECT [DealerCode],[Model],ImeiOne,ImeiTwo,[Color], 0 IsDistributed, 0 IsSoldOut, 
                                                        1 IsReceived, [DistributionDate] ReceiveDate, getdate() AddedDate
                                                 FROM [MDS].[dbo].[Query$]  where ImeiOne not in (  
                                                        SELECT q.ImeiOne
                                                        FROM [MDS].[dbo].[Query$] q
                                                    join
                                                        MDS.dbo.DealerDistribution d
                                                        on q.ImeiOne = d.ImeiOne )
                                                    and ImeiOne is not null 
                                                    and ImeiTwo is not null ");
                var data = await _context.Database.SqlQuery<WDMS_INSERT_IMEIViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<int> SaveAllDataToMDMS()
        {
            int rowsAffected = 0;
            //raihan
            _context.Database.CommandTimeout = 6000;
            string Sql = string.Format(@" insert into MDS.dbo.DealerDistribution(DealerCode, Model, ImeiOne, ImeiTwo,Color, IsDistributed, IsSoldOut, IsReceived, ReceiveDate, AddedDate)
                                          SELECT [DealerCode],[Model],ImeiOne,ImeiTwo,[Color], 0 IsDistributed, 0 IsSoldOut, 1 IsReceived, 
                                                 [DistributionDate] ReceiveDate, getdate() AddedDate
                                          FROM [MDS].[dbo].[Query$]  where ImeiOne not in (  
                                                    SELECT q.ImeiOne
                                                    FROM [MDS].[dbo].[Query$] q
                                                 join
                                                    MDS.dbo.DealerDistribution d
                                                    on q.ImeiOne = d.ImeiOne )
                                              and ImeiOne is not null and ImeiTwo is not null ");
            rowsAffected = await _context.Database.ExecuteSqlCommandAsync(Sql);
            return rowsAffected;

        }

        public async Task<List<SR_IncentiveReportViewModel>> GetSRIncentiveReportData(DateTime fromDate, DateTime todate)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT d.DigitechCode, d.Zone, d.DistributorNameCellCom, sr.Name, sr.Phone, sr.PaymentNumber, 
                                                sr.PaymentNumberType, rd.Model, rd.ImeiOne, rd.ImeiTwo, rd.IsReturn, i.SRAmount 
                                                --select count(1)
	                                                FROM MDS.dbo.RetailerOrder r
	                                                join MDS.dbo.RetailerOrderDetail rod
	                                                on r.Id = rod.OrderId
	                                                join MDS.dbo.RetailerDistribution rd
	                                                on rod.Id = rd.RetailerOrderDetailId
	                                                join MDS.dbo.IncentiveDistribution i
	                                                on rd.Model = i.ModelName
	                                                join MDS.dbo.Distributors d on (r.DealerCode = d.DigitechCode)
	                                                join MDS.dbo.SalesRepresentatives sr on r.SRId = sr.Id
	                                                where convert(date, rd.DistributionDate) between convert(date, '{0}') and convert(date, '{1}') 
	                                                and sr.IsActive = 1 
	                                                AND rd.IsReturn = 0  ", fromDate, todate);
                var data = await _context.Database.SqlQuery<SR_IncentiveReportViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<MisIncentiveViewModel>> GetMisIncentiveReportData(DateTime fromDate, DateTime todate)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" select d.DistributorNameCellCom, d.DigitechCode, d.ImportCode, d.Zone, m.MisName, m.MobileNumber, m.PaymentNumber, 
                                                rd.Model, rd.ImeiOne,rd.ImeiTwo
                                                from 
                                                (SELECT * FROM MDS.dbo.RetailerOrder where convert(date, OrderDate) between convert(date, '{0}') and convert(date, '{1}')) ro
                                                join mds.dbo.RetailerOrderDetail rod on ro.Id = rod.OrderId
                                                join mds.dbo.RetailerDistribution rd on rod.Id = rd.RetailerOrderDetailId
                                                join mds.dbo.Distributors d on ro.DealerCode = d.DigitechCode
                                                join (select * from MDS.dbo.MisPersons where IsActive = 1) m on d.DigitechCode = m.DealerDigitechCode
                                                group by d.DistributorNameCellCom, d.DigitechCode, d.ImportCode, d.Zone, m.MisName, m.MobileNumber, m.PaymentNumber
                                                ,rd.Model,rd.ImeiOne,rd.ImeiTwo ", fromDate, todate);
                var data = await _context.Database.SqlQuery<MisIncentiveViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<Retailer_IncentiveReportViewModel>> GetRetailerIncentiveReportData(DateTime fromDate, DateTime todate,string ddlType)
        {
            try
            {
                string whereExt = string.Empty;

                if (ddlType.ToUpper().Trim() == "1")
                {
                    whereExt = " AND d.DistributorTypeId ='" + ddlType + "' ";

                }
                else if (ddlType.ToUpper().Trim() == "2")
                {
                    whereExt = " AND d.DistributorTypeId ='" + ddlType + "' ";

                }
                else if (ddlType.ToUpper().Trim() == "-1")
                {
                    whereExt = " ";

                }

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT d.DigitechCode,d.DistributorNameCellCom,d.Zone,retailer.RetailerName,convert(NVARCHAR(500), retailer.RetailerAddress) RetailerAddress
	                                                                ,retailer.PhoneNumber,retailer.PaymentNumber,retailer.PaymentNumberType,rd.Model,rd.ImeiOne,rd.ImeiTwo,rd.IsReturn,i.RetailerAmount
                                                                FROM MDS.dbo.RetailerOrder r
                                                                JOIN MDS.dbo.RetailerOrderDetail rod ON r.Id = rod.OrderId
                                                                JOIN MDS.dbo.RetailerDistribution rd ON rod.Id = rd.RetailerOrderDetailId
                                                                JOIN MDS.dbo.IncentiveDistribution i ON rd.Model = i.ModelName
                                                                JOIN MDS.dbo.Distributors d ON (r.DealerCode = d.DigitechCode)
                                                                JOIN MDS.dbo.RetailerInfo retailer ON r.RetailerId = retailer.Id
                                                                WHERE convert(DATE, rd.DistributionDate) BETWEEN convert(DATE, '{0}') AND convert(DATE, '{1}')
	                                                                AND retailer.IsActive = 1
	                                                                AND rd.IsReturn = 0  {2} ", fromDate, todate,whereExt);
                var data = await _context.Database.SqlQuery<Retailer_IncentiveReportViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<DealerTypeViewModel>> GetAllDistributorType()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select DISTINCT DistributorTypeId from Distributors  ");
                var data = await _context.Database.SqlQuery<DealerTypeViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
            
        }

    }
}