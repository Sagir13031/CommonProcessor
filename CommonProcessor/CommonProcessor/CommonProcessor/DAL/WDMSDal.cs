﻿using CommonProcessor.Models.DbContext;
using CommonProcessor.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CommonProcessor.DAL
{
    public class WDMSDal
    {

        #region Common
        private readonly WDSEntities _context;

        public WDMSDal(WDSEntities context)
        {
            _context = context;
        }


        private SqlConnection GetConnectionSql()
        {
            //var connectionStringName = ConfigurationManager.ConnectionStrings["RBSYNERGYEntities"];
            //string connectionString = connectionStringName.ConnectionString;
            const string connectionString = @"Data Source =103.147.182.250; Initial Catalog = WDS; user id=sa;password=Wal_ton@20@20;";
            try
            {
                SqlConnection conCheck = new SqlConnection(connectionString);
                return new SqlConnection(connectionString);
            }
            catch (Exception e)
            {
                throw new Exception("Error : " + e.Message);
            }
        }


        #endregion

        public int ProcessImeiDate(List<WDMS_Dealer_DistributionViewModel> dealerList)
        {
            int copyData = 0;
            try
            {
                truncateQueryTable();//Frist Truncate QueryTable Then Add Data
                DataTable tbl = new DataTable();
                tbl.Columns.Add(new DataColumn("DealerCode", typeof(string)));
                tbl.Columns.Add(new DataColumn("Model", typeof(string)));
                tbl.Columns.Add(new DataColumn("ImeiOne", typeof(string)));
                tbl.Columns.Add(new DataColumn("ImeiTwo", typeof(string)));
                tbl.Columns.Add(new DataColumn("Color", typeof(string)));
                tbl.Columns.Add(new DataColumn("DistributionDate", typeof(string)));
                tbl.Columns.Add(new DataColumn("AddedBy", typeof(string)));

                for (int i = 0; i < dealerList.Count; i++)
                {
                    DataRow dr = tbl.NewRow();
                    dr["DealerCode"] = dealerList[i].DealerCode;
                    dr["Model"] = dealerList[i].Model;
                    dr["ImeiOne"] = dealerList[i].ImeiOne;
                    dr["ImeiTwo"] = dealerList[i].ImeiTwo;
                    dr["Color"] = dealerList[i].Color;
                    //dr["DistributionDate"] = dealerList[i].DistributionDate;
                    dr["DistributionDate"] = Convert.ToDateTime(dealerList[i].DistributionDate).ToString("yyyy-MM-dd");
                    dr["AddedBy"] = dealerList[i].AddedBy;
                    tbl.Rows.Add(dr);
                }
                using (SqlConnection con = GetConnectionSql())
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "[WDS].[dbo].[Query$]";

                        //[OPTIONAL]: Map the Excel columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("DealerCode", "DealerCode");
                        sqlBulkCopy.ColumnMappings.Add("Model", "Model");
                        sqlBulkCopy.ColumnMappings.Add("ImeiOne", "ImeiOne");

                        sqlBulkCopy.ColumnMappings.Add("ImeiTwo", "ImeiTwo");
                        sqlBulkCopy.ColumnMappings.Add("Color", "Color");

                        sqlBulkCopy.ColumnMappings.Add("DistributionDate", "DistributionDate");
                        sqlBulkCopy.ColumnMappings.Add("AddedBy", "AddedBy");

                        con.Open();
                        sqlBulkCopy.WriteToServer(tbl);
                        con.Close();
                        con.Dispose();

                        copyData = tbl.Rows.Count;
                    }
                }
                return copyData;
            }
            catch (Exception e)
            {
                return copyData;
            }

        }

        private int truncateQueryTable()
        {
            int rowsAffected = 0;
            try
            {
                _context.Database.CommandTimeout = 6000;
                string Sql = "TRUNCATE TABLE [WDS].[dbo].[Query$] ";

                rowsAffected = _context.Database.ExecuteSqlCommand(Sql);
                return rowsAffected;
            }
            catch (Exception e)
            {
                return rowsAffected;
            }

        }

        public async Task<List<WDMS_IMEI_EXIST>> GetExistData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select x.DealerCode dDealerCode, x.ImeiOne dImeiOne, ddd.DealerCode dddDealerCode, ddd.ReceiveDate from (
                                                    SELECT [DealerCode],[Model],ImeiOne,ImeiTwo,[Color]
                                                  FROM [WDS].[dbo].[Query$]  where ImeiOne in (
                                                        SELECT q.ImeiOne FROM [WDS].[dbo].[Query$] q
                                                      join
                                                        wds.dbo.DealerDistribution d
                                                        on q.ImeiOne = d.ImeiOne 
                                                      )
                                                    )x
                                                    join WDS.dbo.DealerDistribution ddd
                                                    on x.ImeiOne = ddd.ImeiOne ");
                var data = await _context.Database.SqlQuery<WDMS_IMEI_EXIST>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<int> UpdateWDMSTableDate()
        {
            int rowsAffected = 0;
            _context.Database.CommandTimeout = 6000;
            string Sql = string.Format(@" UPDATE WDS.dbo.DealerDistribution set DealerCode = y.dDealerCode, ReceiveDate = getdate() - 1
                                            from(
                                            select x.DealerCode dDealerCode, x.ImeiOne dImeiOne, ddd.DealerCode dddDealerCode, ddd.ReceiveDate from (
                                                SELECT[DealerCode],[Model], ImeiOne, ImeiTwo,[Color]
                                                FROM [WDS].[dbo].[Query$] where ImeiOne in (
                                                    SELECT q.ImeiOne
                                                    FROM [WDS].[dbo].[Query$] q
                                                join
                                                    wds.dbo.DealerDistribution d
                                                    on q.ImeiOne = d.ImeiOne
                                                        )
                                                    )x
                                            join WDS.dbo.DealerDistribution ddd
                                            on x.ImeiOne = ddd.ImeiOne
                                            )y
                                            where ImeiOne = y.dImeiOne ");
            rowsAffected = await _context.Database.ExecuteSqlCommandAsync(Sql);
            return rowsAffected;

        }

        public async Task<List<WDMS_INSERT_IMEIViewModel>> GetAllIMEIDataForWDMS()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@"  SELECT [DealerCode],[Model],ImeiOne,ImeiTwo,[Color], 0 IsDistributed, 0 IsSoldOut, 
                                                        1 IsReceived, [DistributionDate] ReceiveDate, getdate() AddedDate
                                                 FROM [WDS].[dbo].[Query$]  where ImeiOne not in (  
                                                        SELECT q.ImeiOne
                                                        FROM [WDS].[dbo].[Query$] q
                                                    join
                                                        wds.dbo.DealerDistribution d
                                                        on q.ImeiOne = d.ImeiOne )
                                                    and ImeiOne is not null 
                                                    and ImeiTwo is not null ");
                var data = await _context.Database.SqlQuery<WDMS_INSERT_IMEIViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<int> SaveAllDataToWDMS()
        {
            int rowsAffected = 0;
            //raihan
            _context.Database.CommandTimeout = 6000;
            string Sql = string.Format(@" insert into wds.dbo.DealerDistribution(DealerCode, Model, ImeiOne, ImeiTwo,Color, IsDistributed, IsSoldOut, IsReceived, ReceiveDate, AddedDate)
                                          SELECT [DealerCode],[Model],ImeiOne,ImeiTwo,[Color], 0 IsDistributed, 0 IsSoldOut, 1 IsReceived, 
                                                 [DistributionDate] ReceiveDate, getdate() AddedDate
                                          FROM [WDS].[dbo].[Query$]  where ImeiOne not in (  
                                                    SELECT q.ImeiOne
                                                    FROM [WDS].[dbo].[Query$] q
                                                 join
                                                    wds.dbo.DealerDistribution d
                                                    on q.ImeiOne = d.ImeiOne )
                                              and ImeiOne is not null and ImeiTwo is not null ");
            rowsAffected = await _context.Database.ExecuteSqlCommandAsync(Sql);
            return rowsAffected;

        }

        public async Task<List<SR_IncentiveReportViewModel>> GetSRIncentiveReportData(DateTime fromDate, DateTime todate)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT d.DigitechCode, d.Zone, d.DistributorNameCellCom, sr.Name, sr.Phone, sr.PaymentNumber, 
                                                sr.PaymentNumberType, rd.Model, rd.ImeiOne, rd.ImeiTwo, rd.IsReturn, i.SRAmount 
                                                --select count(1)
	                                                FROM WDS.dbo.RetailerOrder r
	                                                join WDS.dbo.RetailerOrderDetail rod
	                                                on r.Id = rod.OrderId
	                                                join WDS.dbo.RetailerDistribution rd
	                                                on rod.Id = rd.RetailerOrderDetailId
	                                                join WDS.dbo.IncentiveDistribution i
	                                                on rd.Model = i.ModelName
	                                                join WDS.dbo.Distributors d on (r.DealerCode = d.DigitechCode)
	                                                join WDS.dbo.SalesRepresentatives sr on r.SRId = sr.Id
	                                                where convert(date, rd.DistributionDate) between convert(date, '{0}') and convert(date, '{1}') 
	                                                and sr.IsActive = 1 
	                                                AND rd.IsReturn = 0  ", fromDate, todate);
                var data = await _context.Database.SqlQuery<SR_IncentiveReportViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<Retailer_IncentiveReportViewModel>> GetRetailerIncentiveReportData(DateTime fromDate, DateTime todate,string ddlType)
        {
            try
            {
                string whereExt = string.Empty;

                if (ddlType.ToUpper().Trim() == "1")
                {
                    whereExt = " AND d.DistributorTypeId ='" + ddlType + "' "; 

                }
                else if (ddlType.ToUpper().Trim() == "2")
                {
                    whereExt = " AND d.DistributorTypeId ='" + ddlType + "' "; 

                }
                else if (ddlType.ToUpper().Trim() == "-1")
                {
                    whereExt = " ";

                }

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT d.DigitechCode,d.DistributorNameCellCom,d.Zone,retailer.RetailerName,convert(NVARCHAR(500), retailer.RetailerAddress) RetailerAddress,
                                                        retailer.PhoneNumber,retailer.PaymentNumber,retailer.PaymentNumberType,rd.Model,rd.ImeiOne,rd.ImeiTwo,rd.IsReturn,i.RetailerAmount
                                                                FROM WDS.dbo.RetailerOrder r
                                                                JOIN WDS.dbo.RetailerOrderDetail rod ON r.Id = rod.OrderId
                                                                JOIN WDS.dbo.RetailerDistribution rd ON rod.Id = rd.RetailerOrderDetailId
                                                                JOIN WDS.dbo.IncentiveDistribution i ON rd.Model = i.ModelName
                                                                JOIN WDS.dbo.Distributors d ON (r.DealerCode = d.DigitechCode)
                                                                JOIN WDS.dbo.RetailerInfo retailer ON r.RetailerId = retailer.Id
                                                                WHERE convert(DATE, rd.DistributionDate) BETWEEN convert(DATE, '{0}') AND convert(DATE, '{1}')
	                                                                AND retailer.IsActive = 1
	                                                                AND rd.IsReturn = 0 {2}  ", fromDate, todate,whereExt);
                var data = await _context.Database.SqlQuery<Retailer_IncentiveReportViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<DealerTypeViewModel>> GetAllDistributorType()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select DISTINCT DistributorTypeId from Distributors  ");
                var data = await _context.Database.SqlQuery<DealerTypeViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<DistributorStockViewModel>> DistributorStock()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT d.DistributorNameCellCom DistributorName, d.DigitechCode, d.ImportCode, d.Zone, x.Name RSMName, 
                                                Model, pm.ProductyType, sum(case when dd.IsDistributed=0 then 1 else 0 end) PresentStock, sum(case when dd.IsDistributed=0 then mp.Price else 0 end) StockValue FROM dbo.DealerDistribution dd
                                                join dbo.Distributors d on (dd.DealerCode = d.DigitechCode or dd.DealerCode = d.ImportCode)  
                                                join dbo.ProductMaster pm on dd.Model = pm.ProductModel
                                                join dbo.ModelPrices mp on pm.ProductModel = mp.ModelName

                                                left join
                                                (
                                                SELECT ST.EmpId, ST.Name, SZ.ZoneId
                                                  FROM [WDS].[dbo].[SalesTeam] ST
                                                  JOIN wds.dbo.SalesZone sz
                                                  on st.EmpId = sz.EmpId 
                                                  where st.Post = 'RSM'
                                                ) x
                                                on d.ZoneId = x.ZoneId
                                                where dd.IsDistributed=0 and d.DistributorTypeId=1
                                                group by d.DistributorNameCellCom, d.DigitechCode, d.ImportCode, d.Zone, x.Name, 
                                                Model, pm.ProductyType, mp.Price ");
                var data = await _context.Database.SqlQuery<DistributorStockViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<DistributorStockViewModel>> ElectronicsDistributorStock()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT d.DistributorNameCellCom DistributorName, d.DigitechCode, d.ImportCode, d.Zone, x.Name RSMName, 
                                                Model, pm.ProductyType, sum(case when dd.IsDistributed=0 then 1 else 0 end) PresentStock, sum(case when dd.IsDistributed=0 then mp.Price else 0 end) StockValue FROM dbo.DealerDistribution dd
                                                join dbo.Distributors d on (dd.DealerCode = d.DigitechCode or dd.DealerCode = d.ImportCode)  
                                                join dbo.ProductMaster pm on dd.Model = pm.ProductModel
                                                join dbo.ModelPrices mp on pm.ProductModel = mp.ModelName

                                                left join
                                                (
                                                SELECT ST.EmpId, ST.Name, SZ.ZoneId
                                                  FROM [WDS].[dbo].[SalesTeam] ST
                                                  JOIN wds.dbo.SalesZone sz
                                                  on st.EmpId = sz.EmpId 
                                                  where st.Post = 'RSM'
                                                ) x
                                                on d.ZoneId = x.ZoneId
                                                where dd.IsDistributed=0 and d.DistributorTypeId=2
                                                group by d.DistributorNameCellCom, d.DigitechCode, d.ImportCode, d.Zone, x.Name, 
                                                Model, pm.ProductyType, mp.Price ");
                var data = await _context.Database.SqlQuery<DistributorStockViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<RetailerStockViewModel>> RetailerModelWiseStock()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select d.DistributorNameCellCom,d.DigitechCode,ri.RetailerName,rd.RetailerId,rd.Model,COUNT(rd.ImeiOne)StockQTY from wds.dbo.RetailerDistribution rd 
                                                join wds.dbo.RetailerOrderDetail rod on rd.RetailerOrderDetailId=rod.Id
                                                join WDS.dbo.RetailerInfo ri on rd.RetailerId=ri.Id
                                                join WDS.dbo.RetailerOrder ro on  rod.OrderId=ro.Id
                                                join WDS.dbo.Distributors d on ro.DealerCode=d.DigitechCode
                                                where rd.IsReturn=0 and (rd.IsSoldOut = 0 or rd.IsSoldOut is null) and
                                                ri.IsActive=1 and
                                                rd.Model in (
                                                'Primo EF10 (1GB)','Primo F10 (2GB)','Primo GM4 (2GB)','Primo S8 (6GB)','Olvio L2','Olvio L1','Olvio L29','Olvio L28i','Olvio L51','Olvio L50',
                                                'Olvio L53','Olvio ML19','Olvio ML21','Olvio ML24','Olvio ML20','Olvio MM23','Olvio MM24','Olvio MM19j','Olvio P17','Olvio MH21',
                                                'Olvio MH23','Olvio MM26','Olvio MM30','Olvio P16','Olvio P15','Olvio P18','Olvio MH22','Olvio S34'
                                                )
                                                group by d.DistributorNameCellCom,d.DigitechCode,ri.RetailerName,rd.RetailerId,rd.Model
                                                order by d.DigitechCode ");
                var data = await _context.Database.SqlQuery<RetailerStockViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<RetailerListViewModel>> GetAll_WDS_RetailerList()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select ri.Id,ri.RetailerName,ri.RetailerAddress,ri.PhoneNumber from WDS.dbo.RetailerInfo ri where ri.IsActive=1 ");

                var data = await _context.Database.SqlQuery<RetailerListViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

    }
}