﻿using CommonProcessor.Models.DbContext;
using CommonProcessor.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CommonProcessor.DAL
{
    public class RBSYNERGYDal
    {
        #region Common
        private readonly RBSYNERGYEntities _context;

        //private readonly RestaurantDBEntities _contextLocal;
        public RBSYNERGYDal(RBSYNERGYEntities context/*, RestaurantDBEntities contextLocal*/)
        {
            _context = context;
            //_contextLocal = contextLocal;
        }


        private SqlConnection GetConnectionSql()
        {
            //var connectionStringName = ConfigurationManager.ConnectionStrings["RBSYNERGYEntities"];
            //string connectionString = connectionStringName.ConnectionString;
            const string connectionString = @"Data Source =192.168.119.83; Initial Catalog = RBSYNERGY; user id=sa;password=SGR~1806*20;";
            try
            {
                SqlConnection conCheck = new SqlConnection(connectionString);
                return new SqlConnection(connectionString);
            }
            catch (Exception e)
            {
                throw new Exception("Error : " + e.Message);
            }
        }


        #endregion
        public int ProcessImeiDate(List<LogisticViewModel> logisticsList)
        {
            int copyData = 0;
            try
            {
                truncateQueryTable();//Frist Truncate QueryTable Then Add Data
                DataTable tbl = new DataTable();
                tbl.Columns.Add(new DataColumn("BoxCode", typeof(string)));
                tbl.Columns.Add(new DataColumn("Imei1", typeof(string)));
                tbl.Columns.Add(new DataColumn("Imei2", typeof(string)));
                tbl.Columns.Add(new DataColumn("Color", typeof(string)));
                tbl.Columns.Add(new DataColumn("Model", typeof(string)));
                tbl.Columns.Add(new DataColumn("Grade", typeof(string)));

                tbl.Columns.Add(new DataColumn("GradeReason", typeof(string)));
                tbl.Columns.Add(new DataColumn("AddedDate", typeof(string)));
                tbl.Columns.Add(new DataColumn("WO", typeof(string)));
                tbl.Columns.Add(new DataColumn("Delivered", typeof(string)));
                tbl.Columns.Add(new DataColumn("Updated", typeof(string)));


                for (int i = 0; i < logisticsList.Count; i++)
                {
                    DataRow dr = tbl.NewRow();
                    dr["BoxCode"] = logisticsList[i].BoxCode;
                    dr["Imei1"] = logisticsList[i].Imei1;
                    dr["Imei2"] = logisticsList[i].Imei2;
                    dr["Color"] = logisticsList[i].Color;
                    dr["Model"] = logisticsList[i].Model;
                    dr["Grade"] = logisticsList[i].Grade;
                    dr["GradeReason"] = logisticsList[i].GradeReason;
                    dr["AddedDate"] = logisticsList[i].AddedDate;
                    dr["WO"] = logisticsList[i].WO;
                    dr["Delivered"] = logisticsList[i].Delivered;
                    dr["Updated"] = logisticsList[i].Updated;
                    tbl.Rows.Add(dr);
                }
                using (SqlConnection con = GetConnectionSql())
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "[RBSYNERGY].[dbo].[Query]";

                        //[OPTIONAL]: Map the Excel columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("BoxCode", "BoxCode");
                        sqlBulkCopy.ColumnMappings.Add("Imei1", "Imei1");
                        sqlBulkCopy.ColumnMappings.Add("Imei2", "Imei2");

                        sqlBulkCopy.ColumnMappings.Add("Color", "Color");
                        sqlBulkCopy.ColumnMappings.Add("Model", "Model");
                        sqlBulkCopy.ColumnMappings.Add("Grade", "Grade");

                        sqlBulkCopy.ColumnMappings.Add("GradeReason", "GradeReason");
                        sqlBulkCopy.ColumnMappings.Add("AddedDate", "AddedDate");
                        sqlBulkCopy.ColumnMappings.Add("WO", "WO");

                        sqlBulkCopy.ColumnMappings.Add("Delivered", "Delivered");
                        sqlBulkCopy.ColumnMappings.Add("Updated", "Updated");

                        con.Open();
                        sqlBulkCopy.WriteToServer(tbl);
                        con.Close();
                        con.Dispose();

                        copyData = tbl.Rows.Count;
                        int process1 = processGradeAsync1();
                        int process2 = processGradeAsync2();
                    }
                }
                return copyData;
            }
            catch (Exception e)
            {
                return copyData;
            }

        }
        private int processGradeAsync1()
        {
            int rowsAffected = 0;
            string Sql = "UPDATE [RBSYNERGY].[dbo].[Query] SET Grade=REPLACE(Grade, char(9), '');";
            using (SqlConnection sqlConn = GetConnectionSql())
            {
                SqlCommand objSqlCommand = new SqlCommand(Sql, sqlConn);

                try
                {
                    sqlConn.Open();
                    rowsAffected = objSqlCommand.ExecuteNonQuery();
                    sqlConn.Close();
                    if (rowsAffected >= 1)
                    {
                        return rowsAffected;
                    }
                    else
                    {
                        return rowsAffected;
                    }
                }
                catch (Exception ex)
                {
                    return rowsAffected;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                    objSqlCommand.Dispose();
                }
            }

        }
        private int processGradeAsync2()
        {
            int rowsAffected = 0;
            string Sql = "UPDATE [RBSYNERGY].[dbo].[Query] SET Grade=LTRIM(RTRIM(Grade));";
            using (SqlConnection sqlConn = GetConnectionSql())
            {
                SqlCommand objSqlCommand = new SqlCommand(Sql, sqlConn);

                try
                {
                    sqlConn.Open();
                    rowsAffected = objSqlCommand.ExecuteNonQuery();
                    sqlConn.Close();
                    if (rowsAffected >= 1)
                    {
                        return rowsAffected;
                    }
                    else
                    {
                        return rowsAffected;
                    }
                }
                catch (Exception ex)
                {
                    return rowsAffected;
                }
                finally
                {
                    sqlConn.Close();
                    sqlConn.Dispose();
                    objSqlCommand.Dispose();
                }
            }

        }
        public int UpdateMitchMatchData(LogisticViewModel objLogisticViewModel)
        {

            int rowsAffected = 0;
            string Sql = "UPDATE [RBSYNERGY].[dbo].[Query] " +
                "SET [Delivered]='" + objLogisticViewModel.Delivered + "' ," +
                " [Updated] ='" + objLogisticViewModel.Updated + "'" +
                "  WHERE [Imei1] ='" + objLogisticViewModel.Imei1 + "'" +
                "  AND [Imei2] ='" + objLogisticViewModel.Imei2 + "'" +
                "  AND [Model] = '" + objLogisticViewModel.Model + "';";

            rowsAffected = _context.Database.ExecuteSqlCommand(Sql);
            return rowsAffected;


        }
        public async Task<int> UpdateModelMisMatchData(LogisticViewModel objLogisticViewModel)
        {

            int rowsAffected = 0;
            string Sql = "UPDATE [RBSYNERGY].[dbo].[Query] " +
                "SET [Model]='" + objLogisticViewModel.Model + "' " +
                "  WHERE [Imei1] ='" + objLogisticViewModel.Imei1 + "'" +
                "  AND [Imei2] ='" + objLogisticViewModel.Imei2 + "' ; ";
            rowsAffected = await _context.Database.ExecuteSqlCommandAsync(Sql);
            return rowsAffected;
        }
        private int truncateQueryTable()
        {
            int rowsAffected = 0;
            try
            {
                _context.Database.CommandTimeout = 6000;
                string Sql = " TRUNCATE TABLE [RBSYNERGY].[dbo].[Query] ";

                rowsAffected = _context.Database.ExecuteSqlCommand(Sql);
                return rowsAffected;
            }
            catch (Exception e)
            {
                return rowsAffected;
            }

        }
        public async Task<List<LogisticViewModel>> GetCKDSKDMitchMatchData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@"SELECT  [BoxCode]
                                                  ,[Imei1],[Imei2],[Color],[Model],[Grade],[GradeReason]
                                                  ,Cast([AddedDate] as datetime)[AddedDate]
                                                  ,[WO] ,[Delivered]
                                                  ,[Updated]
                                              FROM [RBSYNERGY].[dbo].[Query]  WHERE [Delivered] Not In ( 'SKD','CKD')");
                var data = await _context.Database.SqlQuery<LogisticViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }




            ///Loacl er jonno
            ///




            //var sql = " SELECT  *  FROM [RestaurantDB].[dbo].[Query]  WHERE [Delivered] Not In( 'SKD','CKD') ";

            //using (SqlConnection objConnection = GetConnectionSql())
            //{

            //    using (SqlCommand objCommand = new SqlCommand(sql, objConnection))
            //    {

            //        try
            //        {
            //            objCommand.CommandTimeout = 60;
            //            await objConnection.OpenAsync();
            //            using (SqlDataReader objDataReader = (SqlDataReader)await objCommand.ExecuteReaderAsync())
            //            {
            //                List<LogisticViewModel> objList= new List<LogisticViewModel>();

            //                try
            //                {
            //                    LogisticViewModel objEmployeeModelApi = new LogisticViewModel();
            //                    while (await objDataReader.ReadAsync())
            //                    {
            //                        objEmployeeModelApi.BoxCode = objDataReader["BoxCode"].ToString();
            //                        objEmployeeModelApi.Imei1 = objDataReader["Imei1"].ToString();
            //                        objEmployeeModelApi.Imei2 = objDataReader["Imei2"].ToString();

            //                        objEmployeeModelApi.Color = objDataReader["Color"].ToString();
            //                        objEmployeeModelApi.Model = objDataReader["Model"].ToString();
            //                        objEmployeeModelApi.Grade = objDataReader["Grade"].ToString();
            //                        objEmployeeModelApi.GradeReason = objDataReader["GradeReason"].ToString();
            //                        objEmployeeModelApi.AddedDate = Convert.ToDateTime(objDataReader["AddedDate"].ToString());
            //                        objEmployeeModelApi.WO = objDataReader["WO"].ToString();
            //                        objEmployeeModelApi.Delivered = objDataReader["Delivered"].ToString();
            //                        objEmployeeModelApi.Updated = objDataReader["Updated"].ToString();

            //                        objList.Add(objEmployeeModelApi);

            //                    }
            //                    return objList;
            //                }
            //                catch (Exception ex)
            //                {
            //                    throw new Exception("Error : " + ex.Message);
            //                }
            //                finally
            //                {
            //                    objDataReader.Dispose();
            //                    objCommand.Dispose();
            //                    objConnection.Dispose();
            //                    objConnection.Close();
            //                }
            //            }
            //        }
            //        catch (Exception e)
            //        {

            //            throw new Exception("Error : " + e.Message);
            //        }

            //    }
        }
        //WDMS
        public async Task<List<WDMS_Dealer_DistributionViewModel>> GetDataFromRBSYNERGYForWDMS(DateTime fromDate, DateTime todate)
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" select d.DealerCode, d.Model, d.BarCode ImeiOne, d.BarCode2 ImeiTwo,
                                                    i.Color, d.DistributionDate,d.AddedBy,pm.BrandName from RBSYNERGY.dbo.tblDealerDistributionDetails d
                                                    join RBSYNERGY.dbo.tblBarCodeInv i on d.barcode = i.barcode
                                                    left join tblProductMaster pm on i.Model=pm.ProductModel
                                                    where convert(date, d.DistributionDate) between '" + fromDate + "' and '" + todate + "' and pm.productModel is not null and pm.BrandName='WALTON' ");
                var data = await _context.Database.SqlQuery<WDMS_Dealer_DistributionViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        //MDMS
        public async Task<List<WDMS_Dealer_DistributionViewModel>> GetDataFromRBSYNERGYForMDMS(DateTime fromDate, DateTime todate)
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" select d.DealerCode, d.Model, d.BarCode ImeiOne, d.BarCode2 ImeiTwo,
                                                    i.Color, d.DistributionDate,d.AddedBy,pm.BrandName from RBSYNERGY.dbo.tblDealerDistributionDetails d
                                                    join RBSYNERGY.dbo.tblBarCodeInv i on d.barcode = i.barcode
                                                    left join tblProductMaster pm on i.Model=pm.ProductModel
                                                    where convert(date, d.DistributionDate) between '" + fromDate + "' and '" + todate + "' and pm.productModel is not null and pm.BrandName='MARCEL' ");
                var data = await _context.Database.SqlQuery<WDMS_Dealer_DistributionViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public async Task<List<ModelMisMatchViewModel>> GetModelMisMatchData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@"SELECT a.Model,pm.ProductModel,a.Color,a.Imei1,a.Imei2 FROM [Query] a 
                                                Left Outer Join  tblBarCodeInv inv on a.Imei1=inv.BarCode
                                                left join tblProductMaster pm on a.Model=pm.ProductModel
                                                WHERE pm.productModel is  null");
                var data = await _context.Database.SqlQuery<ModelMisMatchViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }


        }
        public async Task<List<LogisticViewModel>> GetAllModelNameGroupData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@"Select Model, Delivered ,Updated from [Query] group by Model,Delivered ,Updated");
                var data = await _context.Database.SqlQuery<LogisticViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }


        }
        public async Task<List<ImeiExistViewModel>> GetImeiExistData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@"Select q.*
                                                FROM tblBarCodeInv a
                                                inner join [Query] q 
                                                on a.BarCode=q.Imei1");
                var data = await _context.Database.SqlQuery<ImeiExistViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public async Task<List<LogisticViewModel>> GetAllIMEIDataForSaveBarcodeInv()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@"SELECT  [BoxCode]
                                                      ,[Imei1]
                                                      ,[Imei2]
                                                      ,[Color]
                                                      ,[Model]
                                                      ,[Grade]
                                                      ,[GradeReason]
                                                      ,Cast([AddedDate] as datetime)[AddedDate]
                                                      ,[WO]
                                                      ,[Delivered]
                                                      ,[Updated]
                                                  FROM [RBSYNERGY].[dbo].[Query];");
                var data = await _context.Database.SqlQuery<LogisticViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public async Task<int> SaveAllDataToBarcodeInv()
        {
            int rowsAffected = 0;
            //raihan
            _context.Database.CommandTimeout = 6000;
            string Sql = "Insert Into tblBarCodeInv " +
                " (PrintDate, ProductType, Model, Color, BarCode, BarCode2, DelieveredTo, PrintSuccess, AddedBy, DateAdded, Grade,GradeReason, UpdatedBy) " +
                " SELECT CONVERT(DATETIME, Convert(date, a.AddedDate), 102),'Cell Phone',pm.ProductModel,a.Color,a.Imei1,a.Imei2,Delivered,1,'raihan',DATEADD(DAY, 1, Convert(date, AddedDate)),a.Grade,a.GradeReason,a.Updated" +
                "  FROM [RBSYNERGY].[dbo].[Query] a " +
                " Left Outer Join  tblBarCodeInv inv on a.Imei1 = inv.BarCode " +
                " left join tblProductMaster pm on a.Model = pm.ProductModel " +
                " WHERE inv.BarCode IS NULL and pm.productModel is not null; ";

            rowsAffected = await _context.Database.ExecuteSqlCommandAsync(Sql);
            return rowsAffected;

        }
        public int truncateDealerCodeChangeTestTable()
        {
            int rowsAffected = 0;
            try
            {
                _context.Database.CommandTimeout = 6000;
                string Sql = " Truncate table [RBSYNERGY].[dbo].[DealerChangeTest1] ";

                rowsAffected = _context.Database.ExecuteSqlCommand(Sql);
                return rowsAffected;
            }
            catch (Exception e)
            {
                return rowsAffected;
            }

        }
        public async Task<List<BpWiseDealerCodeChangeViewModel>> GetAllImeiBpWiseDealerCodeChangeData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select m.tBarcode as MBarCode,m.tBarcode,m.Phn
                                                    ,Case when m.Model is NULL then n.Model else m.Model end as Model
                                                    ,Case when m.BarCode is NULL then n.BarCode else m.BarCode end as BarCode
                                                    ,Case when m.BarCode2 is NULL then n.BarCode2 else m.BarCode2 end as BarCode2
                                                    ,Case when m.DealerCode is NULL then n.DealerCode else m.DealerCode end as DealerCode,
                                                    bi.BPName,bi.BPPhoneNumber from
                                                    (
                                                    Select a.Barcode as tBarcode,a.Phn,d.BarCode,d.BarCode2,d.DealerCode,d.Model from [dbo].[DealerChangeTest1] a  
                                                    left join RBSYNERGY.dbo.tblDealerDistributionDetails d on a.Barcode=d.BarCode 
                                                    )m
                                                    left join
                                                    (
                                                    Select a.Barcode as tBarcode,a.Phn,d.BarCode,d.BarCode2,d.DealerCode,d.Model from [dbo].[DealerChangeTest1] a  
                                                    left join RBSYNERGY.dbo.tblDealerDistributionDetails d on a.Barcode=d.BarCode2
                                                    )n on m.tBarCode=n.BarCode2
                                                    left join BrandPromotion.[dbo].[BPInfo] bi on m.Phn=bi.BPPhoneNumber ");
                var data = await _context.Database.SqlQuery<BpWiseDealerCodeChangeViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public int UpdateWrongDealerData(BpWiseDealerCodeChangeViewModel objBpWiseDealerCodeChangeViewModel)
        {

            int rowsAffected = 0;
            string Sql = "UPDATE [RBSYNERGY].[dbo].[DealerChangeTest1] " +
                "SET [BarCode]='" + objBpWiseDealerCodeChangeViewModel.tBarCode + "' ," +
                " [Phn] ='" + objBpWiseDealerCodeChangeViewModel.BPPhoneNumber + "'" +
                "  WHERE [BarCode] ='" + objBpWiseDealerCodeChangeViewModel.BarCode + "'";

            rowsAffected = _context.Database.ExecuteSqlCommand(Sql);
            return rowsAffected;

        }        
        public async Task<tblDealerInfo> GetADealerByEbsCode( string DealerCode)
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select  * from tblDealerInfo where EbsNewServerCode ='{0}' and AlternateCode='{0}' order by DealerCode ", DealerCode);
                var data = await _context.Database.SqlQuery<tblDealerInfo>(query).FirstOrDefaultAsync();
                return data;

            }
            catch (Exception)
            {

                return null;
            }
        }
        public async Task<List<Non_WarrantyViewModel>> GetDateWiseNon_Warranty_IMEI(DateTime fromDate, DateTime todate)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" select   NEWID() ProductRegID, 'Cell Phone' ProductType, pm.ProductID ProductModelID,
                                                    BarCode ProductID,'+8801220000000'  PhoneNumber, '1901-12-16' RegistrationDate,12 WarrantyMonths,1 WarrantyVoid,'1. Non Waranty Sale' WarrantyID,
                                                    'Non Warranty App' AddedBy,'{1}' DateAdded,'Non Warranty App' UpdatedBy,'{1}' UpdatedDate,BarCode Imei_One,BarCode2 Imei_Tow
                                                    from(
                                                    Select d.BarCode,d.BarCode2,d.Model,b.Grade,d.DealerCode,d.DistributionDate,r.WarrantyVoid from [RBSYNERGY].[dbo].tblBarCodeInv b 
                                                    inner join [RBSYNERGY].[dbo].tblDealerDistributionDetails d on b.BarCode=d.BarCode
                                                    left join [RBSYNERGY].[dbo].tblProductRegistration r on d.BarCode=r.imei_ONE
                                                    where  b.grade Like '%D%' AND CAST(d.DistributionDate as date) between '{0}' and '{1}' and r.WarrantyVoid IS NULL) o join 
                                                    [RBSYNERGY].[dbo].[tblProductMaster] pm
                                                    on o.Model = pm.ProductModel where pm.ProductModel is not null ", fromDate,todate);

            
                var data = _context.Database.SqlQuery<Non_WarrantyViewModel>(query).ToList();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public int ProcessBarCodeDate(List<DealerChangeTest1> dealerList)
        {
            int copyData = 0;
            try
            {
                truncateDealerCodeChangeTestTable();//Frist Truncate QueryTable Then Add Data
                DataTable tbl = new DataTable();
                tbl.Columns.Add(new DataColumn("Barcode", typeof(string)));

                for (int i = 0; i < dealerList.Count; i++)
                {
                    DataRow dr = tbl.NewRow();
                    dr["Barcode"] = dealerList[i].Barcode;
                    tbl.Rows.Add(dr);
                }
                using (SqlConnection con = GetConnectionSql())
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "[RBSYNERGY].[dbo].[DealerChangeTest1]";

                        //[OPTIONAL]: Map the Excel columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("Barcode", "Barcode");

                        con.Open();
                        sqlBulkCopy.WriteToServer(tbl);
                        con.Close();
                        con.Dispose();

                        copyData = tbl.Rows.Count;
                    }
                }
                return copyData;
            }
            catch (Exception e)
            {
                return copyData;
            }

        }

        public async Task<List<ExcelWiseReportViewModel>> GetExcelWiseReportData()
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select bi.BarCode,bi.BarCode2,bi.Model,bi.Color,bi.Grade,bi.GradeReason 
                                                from RBSYNERGY.[dbo].[DealerChangeTest1] a 
                                                left join RBSYNERGY.dbo.tblBarCodeInv bi  on a.BarCode=bi.Barcode 
                                                where a.Barcode is not null ");
                var data = await _context.Database.SqlQuery<ExcelWiseReportViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<BPSalesDealerListViewModel>> GetAllModelList()
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select ProductModel as Model from tblProductMaster 
                                                where ProductModel like 'Primo%' or ProductModel like 'Olvio%' 
                                                and CAST(DateAdded as date)>='2019-01-01'
                                                order by ProductModel ");

                var data = await _context.Database.SqlQuery<BPSalesDealerListViewModel>(query).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<ExcelWiseReportViewModel> GetImei1Info(string Barcode)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" Select * from RBSYNERGY.dbo.tblBarCodeInv where BarCode='{0}' or BarCode2='{0}' ", Barcode);
                var data = await _context.Database.SqlQuery<ExcelWiseReportViewModel>(query).FirstOrDefaultAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<List<BarCodeScannerViewModel>> GetBarcodeInformation(List<BarCodeScannerViewModel> modelData)
        {
            var list = new List<BarCodeScannerViewModel>();
            try
            {
                foreach (var item in modelData)
                {
                    _context.Database.CommandTimeout = 6000;
                    string query = string.Format(@" SELECT inv.Barcode,inv.barcode2,
                                                inv.Model,Convert(date,c.AddedDate) AS Sale_Date,c.Addedby,dis.DealerCode,i.DealerName,
                                                info.BPPhoneNumber,info.BPName,info.RetailerName,case when repI.Imei_1 is null then '' else 'Replacement Issue'  end as Replacement,a.Registrationdate
                                                FROM (Select * from tblBarCodeInv where Barcode = '{0}'
                                                union 
                                                Select * from tblBarCodeInv where BarCode2 = '{0}'
                                                )inv 
                                                left join rbsynergy.dbo.tblDealerDistributionDetails dis on (dis.barcode=inv.barcode)
                                                left join rbsynergy.dbo.tblProductRegistration a  on inv.barcode=a.Imei_One
                                                left join rbsynergy.dbo.tblDealerInfo i on dis.DealerCode=i.DealerCode
                                                left join BrandPromotion.dbo.BpSales c on (inv.barcode = c.Imei or inv.barcode2 = c.Imei)
                                                left join BrandPromotion.dbo.BpInfo info on (c.BpPhoneMumber = info.BPPhoneNumber)
                                                LEFT OUTER JOIN (Select distinct rep.IMEI_1 from [RBSYNERGY].[IMEIReplacementMaster].[IMEIReplacementMaster] rep 
                                                INNER JOIN BrandPromotion.[dbo].BPSales sale on rep.IMEI_1=sale.Imei
                                                where RequestStatus in ('WastageReceived','DeliveredToRework','ReworkReceived','Approved')) repI on inv.BarCode=repI.IMEI_1 ", item.Barcode);
                    var data = await _context.Database.SqlQuery<BarCodeScannerViewModel>(query).FirstOrDefaultAsync();
                    list.Add(data);
                }


                return list;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<BarCodeInfoViewModel> GetBarcodeInfoDealerWise(string Barcode)
        {
            try
            {

                _context.Database.CommandTimeout = 6000;
                string query = string.Format(@" SELECT inv.Barcode,inv.BarCode2,
                                                inv.Model,inv.Color,dis.DealerCode,i.DealerName,dis.DistributionDate,
                                                a.PhoneNumber as ActivationPhone,a.Registrationdate As ActivationDate
                                                FROM (Select * from tblBarCodeInv where Barcode = '{0}'
                                                union 
                                                Select * from tblBarCodeInv where BarCode2 = '{0}'
                                                )inv 
                                                left join rbsynergy.dbo.tblDealerDistributionDetails dis on (dis.barcode=inv.barcode)
                                                left join rbsynergy.dbo.tblProductRegistration a  on inv.barcode=a.Imei_One
                                                left join rbsynergy.dbo.tblDealerInfo i on dis.DealerCode=i.DealerCode ", Barcode);
                var data = await _context.Database.SqlQuery<BarCodeInfoViewModel>(query).FirstOrDefaultAsync();
                return data;
            }
            catch (Exception e)
            {
                return null;
            }

        }

    }
}