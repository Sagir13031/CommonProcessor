﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class ActiveBPListViewModel
    {
        public long rn { get; set; }
        public string District { get; set; }
        public string DistributorName { get; set; }
        public string DistributorCode { get; set; }
        public string AlternateDistributorCode { get; set; }
        public string RetailerName { get; set; }
        public string RetailerAddress { get; set; }
        public string bpname { get; set; }
        public string bpphonenumber { get; set; }
    }
}