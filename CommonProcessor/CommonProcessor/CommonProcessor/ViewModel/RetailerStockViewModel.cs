﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class RetailerStockViewModel
    {
        public string DistributorNameCellCom { get; set; }
        public string DigitechCode { get; set; }
        public string RetailerName { get; set; }
        public long RetailerId { get; set; }
        public string Model { get; set; }
        public int StockQTY { get; set; }
    }
}