﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class ExcelWiseReportViewModel
    {
        public string BarCode { get; set; }
        public string BarCode2 { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Grade { get; set; }
        public string GradeReason { get; set; }
    }
}