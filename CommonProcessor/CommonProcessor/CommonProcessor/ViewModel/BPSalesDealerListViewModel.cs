﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class BPSalesDealerListViewModel
    {
        public string DealerCode { get; set; }
        public string DealerName { get; set; }
        public string Model { get; set; }
    }
}