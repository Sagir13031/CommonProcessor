﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class WDMS_IMEI_EXIST
    {
        public string dDealerCode { get; set; }
        public string dImeiOne { get; set; }
        public string dddDealerCode { get; set; }
        public DateTime? ReceiveDate { get; set; }
    }
}