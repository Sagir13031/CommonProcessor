﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class ModelMisMatchViewModel
    {
        public string Model { get; set; }
        public string ProductModel { get; set; }
        public string Color { get; set; }
        public string Imei1 { get; set; }
        public string Imei2 { get; set; }
    }
}