﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class MisIncentiveViewModel
    {
        public string DistributorNameCellCom { get; set; }
        public string DigitechCode { get; set; }
        public string ImportCode { get; set; }
        public string Zone { get; set; }
        public string MisName { get; set; }
        public string MobileNumber { get; set; }
        public string PaymentNumber { get; set; }
        public string Model { get; set; }
        public string ImeiOne { get; set; }
        public string ImeiTwo { get; set; }
    }
}