﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class BarCodeInfoViewModel
    {
        public string Barcode { get; set; }
        public string BarCode2 { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string DealerCode { get; set; }
        public string DealerName { get; set; }
        public DateTime? DistributionDate { get; set; }
        public string ActivationPhone { get; set; }
        public DateTime? ActivationDate { get; set; }
    }
}