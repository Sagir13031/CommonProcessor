﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class LogisticViewModel
    {
        public string BoxCode { get; set; }
        public string Imei1 { get; set; }
        public string Imei2 { get; set; }
        public string Color { get; set; }
        public string Model { get; set; }
        public string Grade { get; set; }
        public string GradeReason { get; set; }
        public DateTime AddedDate { get; set; }
        public string WO { get; set; }
        public string Delivered { get; set; }
        public string Updated { get; set; }
    }
}