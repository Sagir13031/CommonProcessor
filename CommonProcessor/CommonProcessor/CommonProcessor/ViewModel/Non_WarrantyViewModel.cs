﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class Non_WarrantyViewModel
    {
        public Guid ProductRegID { get; set; }
        public string ProductType { get; set; }
        public Guid ProductModelID { get; set; }
        public string ProductID { get; set; }
        public string PhoneNumber { get; set; }
        public string RegistrationDate { get; set; }
        public int? WarrantyMonths { get; set; }
        public int? WarrantyVoid { get; set; }
        public string WarrantyID { get; set; }
        public string AddedBy { get; set; }
        public string DateAdded { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string Imei_One { get; set; }
        public string Imei_Tow { get; set; }
    }
}