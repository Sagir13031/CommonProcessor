﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class BpWiseSalesQuantityViewModel
    {
        public string DealerCode { get; set; }
        public string DealerName { get; set; }
        public string BpPhoneMumber { get; set; }
        public string BPName { get; set; }
        public int SALES_QTY { get; set; }
    }
}