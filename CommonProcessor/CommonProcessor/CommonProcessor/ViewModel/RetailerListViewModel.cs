﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class RetailerListViewModel
    {
        public long Id { get; set; }
        public string RetailerName { get; set; }
        public string RetailerAddress { get; set; }
        public string PhoneNumber { get; set; }
    }
}