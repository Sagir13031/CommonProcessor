﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class DistributorStockViewModel
    {
        public string DistributorName { get; set; }
        public string DigitechCode { get; set; }
        public string ImportCode { get; set; }
        public string Zone { get; set; }
        public string RSMName { get; set; }
        public string Model { get; set; }
        public string ProductyType { get; set; }
        public int PresentStock { get; set; }
        public decimal StockValue { get; set; }
    }
}