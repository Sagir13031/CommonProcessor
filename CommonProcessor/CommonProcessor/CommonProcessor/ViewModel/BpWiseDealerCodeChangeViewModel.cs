﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class BpWiseDealerCodeChangeViewModel
    {
        public string DealerCode { get; set; }
        public string MBarCode { get; set; }
        public string tBarCode { get; set; }
        public string Phn { get; set; }
        public string BarCode { get; set; }
        public string BarCode2 { get; set; }
        public string Model { get; set; }
        public string BpName { get; set; }
        public string BPPhoneNumber { get; set; }
    }
}