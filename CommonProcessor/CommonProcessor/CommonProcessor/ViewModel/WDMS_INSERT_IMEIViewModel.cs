﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class WDMS_INSERT_IMEIViewModel
    {
        public string DealerCode { get; set; }
        public string Model { get; set; }
        public string ImeiOne { get; set; }
        public string ImeiTwo { get; set; }
        public string Color { get; set; }
        public int? IsDistributed { get; set; }
        public int? IsSoldOut { get; set; }
        public int? IsReceived { get; set; }
        public string ReceiveDate { get; set; }
        public DateTime AddedDate { get; set; }
    }
}