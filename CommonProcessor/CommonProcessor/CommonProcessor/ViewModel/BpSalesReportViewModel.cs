﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class BpSalesReportViewModel
    {
        public long rn { get; set; }
        public string Imei { get; set; }
        public string Imei2 { get; set; }
        public string Model { get; set; }
        public decimal MSRP { get; set; }
        public string BPName { get; set; }
        public string BpPhoneMumber { get; set; }
        public string RetailerName { get; set; }
        public string RetailerAddress { get; set; }
        public string DealerCode { get; set; }
        public string DealerName { get; set; }
        public DateTime Claim_Date { get; set; }
        public string Replacement { get; set; }
        public DateTime? RegistrationDate { get; set; }
    }
}