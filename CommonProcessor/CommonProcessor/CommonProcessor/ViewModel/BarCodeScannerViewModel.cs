﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class BarCodeScannerViewModel
    {
      public string Barcode { get; set; }
      public string BarCode2 { get; set; }
      public string Model { get; set; }
      public DateTime? Sale_Date {get;set;}
      public string Addedby {get;set;}
      public string DealerCode {get;set;}
      public string DealerName {get;set;}
      public string BPPhoneNumber {get;set;}
      public string BPName {get;set;}
      public string RetailerName {get;set;}
      public string Replacement {get;set;}
      public DateTime? Registrationdate {get;set;}
    }
}