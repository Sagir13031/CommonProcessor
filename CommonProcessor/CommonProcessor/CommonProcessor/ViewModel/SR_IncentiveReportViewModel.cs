﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class SR_IncentiveReportViewModel
    {
        public string DigitechCode { get; set; }
        public string Zone { get; set; }
        public string DistributorNameCellCom { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string PaymentNumber { get; set; }
        public string PaymentNumberType { get; set; }
        public string Model { get; set; }
        public string ImeiOne { get; set; }
        public string ImeiTwo { get; set; }
        public bool IsReturn { get; set; }
        public decimal SRAmount { get; set; }
    }
}