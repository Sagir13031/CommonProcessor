﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class WDMS_Dealer_DistributionViewModel
    {
        public string DealerCode { get; set; }
        public string Model { get; set; }
        public string ImeiOne { get; set; }
        public string ImeiTwo { get; set; }
        public string Color { get; set; }
        public DateTime DistributionDate { get; set; }
        public long? AddedBy { get; set; }
    }
}