﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class Retailer_IncentiveReportViewModel
    {
        public string DigitechCode { get; set; }
        public string DistributorNameCellCom { get; set; }
        public string Zone { get; set; }
        public string RetailerName { get; set; }
        public string RetailerAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string PaymentNumber { get; set; }
        public string PaymentNumberType { get; set; }
        public string Model { get; set; }
        public string ImeiOne { get; set; }
        public string ImeiTwo { get; set; }
        public bool IsReturn { get; set; }
        public decimal RetailerAmount { get; set; }
    }
}