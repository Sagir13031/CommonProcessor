﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonProcessor.ViewModel
{
    public class Non_Warranty_IMEI_Processor_ViewModel
    {

        public string Model { get; set; }
        public string UpdatedBy { get; set; }
        public string DelieveredTo { get; set; }
        public string Grade { get; set; }
        public string IMEI1 { get; set; }
        public string IMEI2 { get; set; }
        public string Color { get; set; }

    }
}