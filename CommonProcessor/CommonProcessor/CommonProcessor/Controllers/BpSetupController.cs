﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class BpSetupController : Controller
    {
        //private readonly WDMSDal _wDMSDal;
        private readonly BpPromotionDal _brandPromotionDal;
        private readonly RBSYNERGYDal _rBSYNERGYDal;
        public BpSetupController()
        {
            //_wDMSDal = new WDMSDal(new WDSEntities());
            _brandPromotionDal = new BpPromotionDal(new BrandPromotionEntities());
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities()/*, new RestaurantDBEntities()*/);

        }

        // GET: BpSetup
        [SessionCheck]
        public ActionResult Index()
        {
            //GetAllActiveBP();
            return View();
        }

        public async Task<ActionResult> GetABpByMobileNumber(string BPPhoneNumber)
        {
            var data = await _brandPromotionDal.GetABpByMobileNumber(BPPhoneNumber);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> GetADealerByEbsCode(string DealerCode)
        {
            var data = await _rBSYNERGYDal.GetADealerByEbsCode(DealerCode);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> GetAllActiveBP()
        {
            var data = await _brandPromotionDal.GetAllActiveBP();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> AddOrEditBP(string EbsCode, string DealerCode, string DealerName, string District, string RetailerID, string RetailerName, string RetailerAddress, string RetailerPhoneNumber, string BpPhoneNumber, string BpName, bool IsActive)//Start to WORK
        {
            var data = await _brandPromotionDal.AddOrEditBP(EbsCode, DealerCode, DealerName, District, RetailerID, RetailerName, RetailerAddress, RetailerPhoneNumber, BpPhoneNumber, BpName, IsActive);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            //return null;

        }
    }
}