﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class BarCodeInfoController : Controller
    {
        // GET: BarCodeInfo
        private readonly RBSYNERGYDal _rBSYNERGYDal;

        public BarCodeInfoController()
        {
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities());

        }

        // GET: BarCodeScanner
        [SessionCheck]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetBarcodeInfoDealerWise(string barCode)
        {
            var data = await _rBSYNERGYDal.GetBarcodeInfoDealerWise(barCode);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
    }
}