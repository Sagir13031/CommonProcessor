﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class DistributorStockController : Controller
    {
        private readonly WDMSDal _wDMSDal;
        private readonly RBSYNERGYDal _rBSYNERGYDal;

        public DistributorStockController()
        {
            _wDMSDal = new WDMSDal(new WDSEntities());
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities()/*, new RestaurantDBEntities()*/);

        }
        // GET: DistributorStock

        [SessionCheck]
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> DistributorStock()
        {
            var data = await _wDMSDal.DistributorStock();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        [SessionCheck]
        public async Task<ActionResult> Electronics_DistributorStock()
        {
            return View();
        }

        public async Task<ActionResult> ElectronicsDistributorStock()
        {
            var data = await _wDMSDal.ElectronicsDistributorStock();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        [SessionCheck]
        public async Task<ActionResult> RetailerModel_WiseStock()
        {
            return View();
        }

        public async Task<ActionResult> RetailerModelWiseStock()
        {
            var data = await _wDMSDal.RetailerModelWiseStock();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
    }
}