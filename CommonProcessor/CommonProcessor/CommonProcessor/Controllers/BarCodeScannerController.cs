﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using CommonProcessor.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class BarCodeScannerController : Controller
    {
        private readonly RBSYNERGYDal _rBSYNERGYDal;

        public BarCodeScannerController()
        {
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities());

        }

        // GET: BarCodeScanner
        [SessionCheck]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetBarcodeInformation(string barCode)
        {
            var modelData = JsonConvert.DeserializeObject<List<BarCodeScannerViewModel>>(barCode);
            
            var data = await _rBSYNERGYDal.GetBarcodeInformation(modelData);            

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
    }
}