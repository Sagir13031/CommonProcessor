﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class BpSalesReportController : Controller
    {
        private readonly BpPromotionDal _brandPromotionDal;

        public BpSalesReportController()
        {
            //_wDMSDal = new WDMSDal(new WDSEntities());
            _brandPromotionDal = new BpPromotionDal(new BrandPromotionEntities());

        }
        // GET: BpSalesReport
        [SessionCheck]
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetBpSalesReportData(DateTime fromDate, DateTime todate)
        {
            var data = await _brandPromotionDal.GetBpSalesReportData(fromDate, todate);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        [SessionCheck]
        [HttpGet]
        public async Task<ActionResult> GetBPPilotReportData()
        {
            return View();
        }

        public async Task<ActionResult> GetBPPilotReportData(DateTime fromDate, DateTime todate, string ddlPhn)
        {
            var data = await _brandPromotionDal.GetBpWiseSalesReportData(fromDate, todate, ddlPhn);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        //[SessionCheck]
        [HttpGet]
        public async Task<ActionResult> GetBPWiseSalesQtyReport()
        {
            return View();
        }

        public async Task<ActionResult> GetBPWiseSalesQtyReport(DateTime fromDate, DateTime todate)
        {
            var data = await _brandPromotionDal.GetBPWiseSalesQtyReport(fromDate, todate);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }
    }
}