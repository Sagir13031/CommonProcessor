﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using CommonProcessor.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class NonWarrantyImeiProcessorController : Controller
    {
        private readonly RBSYNERGYDal _rBSYNERGYDal;

        public NonWarrantyImeiProcessorController()
        {
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities());

        }

        // GET: NonWarrantyImeiProcessor
        [SessionCheck]
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetAllModelList()
        {
            var data = await _rBSYNERGYDal.GetAllModelList();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> GetImei1Info(string Barcode)
        {
            var data = await _rBSYNERGYDal.GetImei1Info(Barcode);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public ActionResult InsertDealer(List<Non_Warranty_IMEI_Processor_ViewModel> modelList)
        {
            string result = "Error! Insert Data Is Not Complete!";
            RBSYNERGYEntities db = new RBSYNERGYEntities();

            if (modelList != null)
            {
                foreach (var item in modelList)
                {
                    var BarCodeInv = db.tblBarCodeInvs.Where(x => x.BarCode == item.IMEI1 || x.BarCode == item.IMEI2).FirstOrDefault();

                    //var DealerDetails = db.tblDealerDistributionDetails.Where(x => x.BarCode == item.BarCode || x.BarCode2 == item.BarCode).FirstOrDefault();

                    //DealerDetails.DealerCode = item.NewDealerCode;

                    if (BarCodeInv == null)
                    {
                        var barCodeInv = new tblBarCodeInv
                        {
                            RecID = Guid.NewGuid(),
                            PrintDate = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now.Date).ToString("yyyy-MM-dd")),
                            ProductType = "Cell Phone",
                            Model = item.Model,
                            Color = item.Color,
                            BarCode = item.IMEI1,
                            BarCode2 = item.IMEI2,
                            DelieveredTo = item.DelieveredTo,
                            PrintSuccess = true,
                            AddedBy = "raihan",
                            DateAdded = Convert.ToDateTime(Convert.ToDateTime(DateTime.Now.Date).ToString("yyyy-MM-dd")),
                            Updatedby = item.UpdatedBy,
                            DateUpdated = DateTime.Now.ToString("yyyyMMdd"),
                            Grade = item.Grade
                        };

                        db.tblBarCodeInvs.Add(barCodeInv);
                        db.SaveChanges();
                        result = "Successfully!! Data Insert Is Complete!";
                    }

                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}