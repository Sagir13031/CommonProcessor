﻿using CommonProcessor;
using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using CommonProcessor.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class MDMS_IMEI_UploadController : Controller
    {
        private readonly MDMSDal _mDMSDal;
        private readonly RBSYNERGYDal _rBSYNERGYDal;
        private readonly WDMSDal _wDMSDal;

        public MDMS_IMEI_UploadController()
        {
            _mDMSDal = new MDMSDal(new MDSEntities());
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities()/*, new RestaurantDBEntities()*/);
            _wDMSDal = new WDMSDal(new WDSEntities());

        }

        // GET: MDMS_IMEI_Upload
        [SessionCheck]
        public ActionResult Index()
        {            
            return View();
        }

        public async Task<ActionResult> GetDataFromRBSYNERGYForMDMS(DateTime fromDate, DateTime todate)
        {
            var data = await _rBSYNERGYDal.GetDataFromRBSYNERGYForMDMS(fromDate, todate);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> SaveMDMSQueryTable(DateTime fromDate, DateTime todate)
        {
            var data = await _rBSYNERGYDal.GetDataFromRBSYNERGYForMDMS(fromDate, todate);
            var SaveDatatoProcessTable = _mDMSDal.ProcessImeiDate(data);

            var returnObj = new
            {
                saveDataCount = SaveDatatoProcessTable,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> GetAllImeiExistData()
        {
            var data = await _mDMSDal.GetExistData();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> UpdateMDMSTable()
        {
            int res = 0;
            try
            {
                res = await _mDMSDal.UpdateMDMSTableDate();
                var returnObj = new
                {
                    Data = res,
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var returnObj = new
                {
                    Data = e.Message.ToString(),
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }

        }

        public async Task<ActionResult> GetAllIMEIDataForMDMS()
        {
            var data = await _mDMSDal.GetAllIMEIDataForMDMS();
            var returnObj = new
            {
                imeiData = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> SaveDataToMDMS()
        {
            int res = 0;
            try
            {
                res = await _mDMSDal.SaveAllDataToMDMS();
                var returnObj = new
                {
                    Data = res,
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var returnObj = new
                {
                    Data = e.Message.ToString(),
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }


        }

        [SessionCheck]
        [HttpGet]
        public async Task<ActionResult> GetSRIncentiveReportData()
        {
            return View();
        }

        [HttpPost]
        [SessionCheck]
        public async Task<ActionResult> GetSRIncentiveReportData(DateTime fromDate, DateTime todate,string ddlBrand)
        {
            var data =new List<SR_IncentiveReportViewModel>();

            if (ddlBrand.ToString() =="WALTON")
            {
                 data = await _wDMSDal.GetSRIncentiveReportData(fromDate, todate);
            }
            else if (ddlBrand.ToString() == "MARCEL")
            {
                data = await _mDMSDal.GetSRIncentiveReportData(fromDate, todate);
            }
            //var data = await _mDMSDal.GetSRIncentiveReportData(fromDate, todate);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        [SessionCheck]
        [HttpGet]
        public async Task<ActionResult> GetMisIncentiveReportData()
        {
            return View();
        }

        [HttpPost]
        //[SessionCheck]
        public async Task<ActionResult> GetMisIncentiveReportData(DateTime fromDate, DateTime todate, string ddlBrand)
        {
            var data = new List<MisIncentiveViewModel>();

            if (ddlBrand.ToString() == "WALTON")
            {
                //data = await _wDMSDal.GetMisIncentiveReportData(fromDate, todate);
            }
            else if (ddlBrand.ToString() == "MARCEL")
            {
                data = await _mDMSDal.GetMisIncentiveReportData(fromDate, todate);
            }
            //var data = await _mDMSDal.GetSRIncentiveReportData(fromDate, todate);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        [SessionCheck]
        [HttpGet]
        public async Task<ActionResult> GetAllDistributorType(string ddlBrand)
        {
            var data = new List<DealerTypeViewModel>();

            if (ddlBrand.ToString() == "1")
            {
                data = await _wDMSDal.GetAllDistributorType();
            }
            else if (ddlBrand.ToString() == "2")
            {
                data = await _mDMSDal.GetAllDistributorType();
            }

            //var data = await _mDMSDal.GetAllDistributorType();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        [SessionCheck]
        public async Task<ActionResult> GetRetailerIncentiveReportData()
        {
            //GetAllDistributorType();
            return View();
        }

        [HttpPost]
        [SessionCheck]
        public async Task<ActionResult> GetRetailerIncentiveReportData(DateTime fromDate, DateTime todate, string ddlBrand,string ddlType)
        {
            var data = new List<Retailer_IncentiveReportViewModel>();

            if (ddlBrand.ToString() == "1")
            {
                data = await _wDMSDal.GetRetailerIncentiveReportData(fromDate, todate,ddlType);
            }
            else
            {
                data = await _mDMSDal.GetRetailerIncentiveReportData(fromDate, todate,ddlType);
            }
            //var data = await _mDMSDal.GetSRIncentiveReportData(fromDate, todate);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

    }
}