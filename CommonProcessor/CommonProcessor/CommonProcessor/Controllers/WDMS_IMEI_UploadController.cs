﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class WDMS_IMEI_UploadController : Controller
    {
        private readonly WDMSDal _wDMSDal;
        private readonly RBSYNERGYDal _rBSYNERGYDal;

        public WDMS_IMEI_UploadController()
        {
            _wDMSDal = new WDMSDal(new WDSEntities());
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities()/*, new RestaurantDBEntities()*/);

        }
        // GET: WDMS_IMEI_Upload

        [SessionCheck]
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetDataFromRBSYNERGYForWDMS(DateTime fromDate, DateTime todate)
        {
            var data = await _rBSYNERGYDal.GetDataFromRBSYNERGYForWDMS(fromDate, todate);

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> SaveWDMSQueryTable(DateTime fromDate, DateTime todate)
        {
            var data = await _rBSYNERGYDal.GetDataFromRBSYNERGYForWDMS(fromDate, todate);
            var SaveDatatoProcessTable = _wDMSDal.ProcessImeiDate(data);

            var returnObj = new
            {
                saveDataCount = SaveDatatoProcessTable,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> GetAllImeiExistData()
        {
            var data = await _wDMSDal.GetExistData();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> UpdateWDMSTable()
        {
            int res = 0;
            try
            {
                res = await _wDMSDal.UpdateWDMSTableDate();
                var returnObj = new
                {
                    Data = res,
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var returnObj = new
                {
                    Data = e.Message.ToString(),
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }

        }

        public async Task<ActionResult> GetAllIMEIDataForWDMS()
        {
            var data = await _wDMSDal.GetAllIMEIDataForWDMS();
            var returnObj = new
            {
                imeiData = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> SaveDataToWDMS()
        {
            int res = 0;
            try
            {
                res = await _wDMSDal.SaveAllDataToWDMS();
                var returnObj = new
                {
                    Data = res,
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                var returnObj = new
                {
                    Data = e.Message.ToString(),
                };
                return Json(returnObj, JsonRequestBehavior.AllowGet);
            }


        }

        public async Task<ActionResult> GetAll_WDS_RetailerList()
        {
            var data = await _wDMSDal.GetAll_WDS_RetailerList();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

    }
}