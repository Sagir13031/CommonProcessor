﻿using CommonProcessor.DAL;
using CommonProcessor.Models.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommonProcessor.Controllers
{
    public class ExcelWiseReportController : Controller
    {
        private readonly RBSYNERGYDal _rBSYNERGYDal;

        public ExcelWiseReportController()
        {
            //_wDMSDal = new WDMSDal(new WDSEntities());
            _rBSYNERGYDal = new RBSYNERGYDal(new RBSYNERGYEntities());

        }

        // GET: ExcelWiseReport
        [SessionCheck]
        public ActionResult Index()
        {
            return View();
        }


        public async Task<ActionResult> SaveTempData(List<DealerChangeTest1> dealerModelList)
        {
            var SaveDatatoProcessTable = _rBSYNERGYDal.ProcessBarCodeDate(dealerModelList);

            var returnObj = new
            {
                saveDataCount = SaveDatatoProcessTable,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        public async Task<ActionResult> GetExcelWiseReportData()
        {
            var data = await _rBSYNERGYDal.GetExcelWiseReportData();

            var returnObj = new
            {
                Data = data,
            };
            return new JsonResult()
            {
                Data = returnObj,
                MaxJsonLength = 86753090,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }


    }
}